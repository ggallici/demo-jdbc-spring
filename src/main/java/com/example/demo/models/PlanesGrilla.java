package com.example.demo.models;

import org.springframework.jdbc.core.RowMapper;

public class PlanesGrilla {
    public static final String consulta =
        "SELECT cuit, razonSocial, esGranContribuyente, cantidadPlanes = COUNT(*), montoConsolidado = SUM(montoConsolidado)\n" +
        "FROM \n" +
        "(\n" +
        "\tSELECT \n" +
        "\tcuit = G0000CUIT,\n" +
        "\trazonSocial = LTRIM(RTRIM(G0000NOMBRERAZON)) + ' ' + LTRIM(RTRIM(G0000APELLIDORAZON)),\n" +
        "\tesGranContribuyente = CASE G0000CATEGORIA WHEN 11003 THEN 'SI' ELSE 'NO' END,\n" +
        "\tmontoConsolidado = G0704TOTVALACTUAL\n" +
        "\tFROM dbpuc..[GIT0000] A\n" +
        "\tJOIN dbpuc..[GIT0704] B ON A.G0000CUIT = B.G0704CUITTITULAR\n" +
        "\tWHERE G0000CUIT = 30677237119\n" +
        ") as TablaAuxiliar\n" +
        "GROUP BY cuit, razonSocial, esGranContribuyente\n";

    public static final RowMapper<PlanesGrilla> rowMapper = (resultSet, rowNumber) -> {
        return new PlanesGrilla(
            resultSet.getString("cuit"),
            resultSet.getString("razonSocial"),
            resultSet.getString("esGranContribuyente"),
            resultSet.getInt("cantidadPlanes"),
            resultSet.getDouble("montoConsolidado")
        );
    };


    private final String cuit;
    private final String razonSocial;
    private final String esGranContribuyente;
    private final Integer cantidadPlanes;
    private final Double montoConsolidado;

    public PlanesGrilla(String cuit, String razonSocial, String esGranContribuyente, Integer cantidadPlanes, Double montoConsolidado) {
        this.cuit = cuit;
        this.razonSocial = razonSocial;
        this.esGranContribuyente = esGranContribuyente;
        this.cantidadPlanes = cantidadPlanes;
        this.montoConsolidado = montoConsolidado;
    }

    public String getCuit() {
        return cuit;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getEsGranContribuyente() {
        return esGranContribuyente;
    }

    public Integer getCantidadPlanes() {
        return cantidadPlanes;
    }

    public Double getMontoConsolidado() {
        return montoConsolidado;
    }
}
