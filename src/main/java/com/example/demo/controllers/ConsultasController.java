package com.example.demo.controllers;

import com.example.demo.models.PlanesGrilla;
import com.example.demo.services.ConsultasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsultasController {
    @Autowired
    private ConsultasService consultasService;

    @GetMapping("/planesGrilla")
    public PlanesGrilla getPlanesGrilla() {
        return consultasService.getPlanesGrilla();
    }
}
