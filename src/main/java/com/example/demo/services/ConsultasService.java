package com.example.demo.services;

import com.example.demo.models.PlanesGrilla;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ConsultasService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public PlanesGrilla getPlanesGrilla() {
        return jdbcTemplate.queryForObject(PlanesGrilla.consulta, PlanesGrilla.rowMapper);
    }
}
